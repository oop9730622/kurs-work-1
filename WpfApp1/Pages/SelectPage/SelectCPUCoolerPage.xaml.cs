﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WpfApp1.Model.DB;

namespace WpfApp1.Pages.SelectPage
{
    public class SelectedCCEventArgs : EventArgs
    {
        public Model.DB.CpuCooler CpuCoolers { get; set; }
        public SelectedCCEventArgs(Model.DB.CpuCooler CpuCoolers)
        {
            this.CpuCoolers = CpuCoolers;
        }
    }
    public partial class SelectCPUCoolerPage : UserControl, INotifyPropertyChanged
    {
        public event EventHandler<SelectedCCEventArgs>? CCSelectedHandler;
        public event PropertyChangedEventHandler? PropertyChanged;
        public void NotifyPropertyChange([CallerMemberName] string? propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        public ObservableCollection<Model.DB.CpuCooler> CCList { get; set; } = [];
        public ComponentsContext ComponentsDB { get; set; }
        private Model.DB.CpuCooler _selectedCC = new() { Id = -1, Name = "None" };
        public Model.DB.CpuCooler SelectedCC 
        {
            get => _selectedCC;
            set
            {
                _selectedCC = value;
                NotifyPropertyChange();
            }
        }
        public SelectCPUCoolerPage()
        {
            DataContext = this;
            InitializeComponent();
            PropertyChanged += OnPropChange;
        }
        private void OnPropChange(object? sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(SelectedCC))
            {
                if (SelectedCC != null && SelectedCC.Id != -1)
                {
                    tbBrand.Text = SelectedCC.Brand;
                    tbType.Text = SelectedCC.Type;
                    tbFanSpeed.Text = SelectedCC.FanSpeed.ToString();
                    tbNoiseLevel.Text = SelectedCC.NoiseLevel.ToString();
                    tbAirflow.Text = SelectedCC.NoiseLevel.ToString();
                    tbDimensions.Text = SelectedCC.Dimensions;
                    tbSoсket.Text = (from socket in ComponentsDB.Sockets where socket.Id == SelectedCC.SocketId select socket).ToList().FirstOrDefault().Name;
                    tbPrice.Text = SelectedCC.Price.ToString();
                    string? img_src = SelectedCC.ImgUrl;
                    if (img_src != null)
                    {
                        ImgUrl.Source = new BitmapImage(new Uri(img_src));
                        ImgUrl.Visibility = Visibility.Visible;
                    }
                    else
                    {
                        ImgUrl.Visibility = Visibility.Hidden;
                    }
                }
                else
                {
                    ClearFields();
                }
            }
           
                
        }

        private void ClearFields()
        {

            tbBrand.Text = string.Empty;
            tbType.Text = string.Empty;
            tbFanSpeed.Text = string.Empty;
            tbNoiseLevel.Text = string.Empty;
            tbAirflow.Text = string.Empty;
            tbDimensions.Text = string.Empty;
            tbSoсket.Text = string.Empty;
            tbPrice.Text = string.Empty;
            ImgUrl.Visibility = Visibility.Hidden;
        }

        public void SetUp(int motherboardId = -1, int CCId = -1)
        {

            CCList.Clear();
            Model.DB.CpuCooler empty = new()
            {
                Id = -1,
                Name = "None"
            };
            CCList.Add(empty);

            CpuCooler sel = null;

            if (motherboardId == -1)
            {
                if (CCId == -1)
                  ListBoxCC.SelectedItem = empty;

                foreach (CpuCooler cpuCooler in ComponentsDB.CpuCooler)
                {
                    CCList.Add(cpuCooler);
                    if (cpuCooler.Id == CCId)
                        sel = cpuCooler;
                }
            }
            else
            {
                if (CCId == -1)
                    ListBoxCC.SelectedItem = empty;

                int socketId = (from mb in ComponentsDB.Motherboards where mb.Id == motherboardId select mb.SocketId).First();
                foreach (CpuCooler cpuCooler in (from cpuCooler in ComponentsDB.CpuCooler where cpuCooler.SocketId == socketId select cpuCooler))
                {
                    CCList.Add(cpuCooler);
                    if (cpuCooler.Id == CCId)
                        sel = cpuCooler;
                }
            }

            if (sel != null)
            {
                SelectedCC = sel;
            }
        }
        private void ButtonChooseComponent_Click(object sender, RoutedEventArgs e)
        {
           CCSelectedHandler?.Invoke(this, new SelectedCCEventArgs(SelectedCC));
        }

        private void ButtonNULLComponent_Click(object sender, RoutedEventArgs e)
        {
            SelectedCC = null;
        }
    }
}
