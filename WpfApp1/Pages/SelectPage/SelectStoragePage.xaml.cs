﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WpfApp1.Model.DB;

namespace WpfApp1.Pages.SelectPage
{
    public class SelectedStorageEventArgs : EventArgs
    {
        public Model.DB.Storage? Storage { get; set; }
        public SelectedStorageEventArgs(Model.DB.Storage? Storage)
        {
            this.Storage = Storage;
        }
    }
    public partial class SelectStoragePage : UserControl, INotifyPropertyChanged
    {
        public event EventHandler<SelectedStorageEventArgs> StorageSelectedHandler;
        public event PropertyChangedEventHandler? PropertyChanged;
        public void NotifyPropertyChange([CallerMemberName] string? propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        public ObservableCollection<Model.DB.Storage> StorageList { get; set; } = [];
        public Model.DB.Storage? _selectedStorage { get; set; }
        public Model.DB.Storage SelectedStorage
        {
            get => _selectedStorage;
            set
            {
                _selectedStorage = value;
                NotifyPropertyChange();
            }
        }
        public ComponentsContext ComponentsDB { get; set; }


        public SelectStoragePage()
        {
            DataContext = this;
            InitializeComponent();
            PropertyChanged += OnPropChange;
        }
        private void OnPropChange(object? sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(SelectedStorage))
            {
                if (SelectedStorage != null&& SelectedStorage.Id != -1)
                {
                    tbBrand.Text = SelectedStorage.Brand;
                    tbCapacity.Text = SelectedStorage.Capacity;
                    tbInterface.Text = SelectedStorage.Interface;
                    tbFormFactor.Text = SelectedStorage.FormFactor;
                    tbReedSpeed.Text = SelectedStorage.ReadSpeed.ToString();
                    tbWriteSpeed.Text = SelectedStorage.WriteSpeed.ToString();
                    tbPrice.Text = SelectedStorage.Price.ToString();

                    string? img_src = SelectedStorage.ImgUrl;
                    if (img_src != null)
                    {
                        ImgUrl.Source = new BitmapImage(new Uri(img_src));
                        ImgUrl.Visibility = Visibility.Visible;
                    }
                    else
                    {
                        ImgUrl.Visibility = Visibility.Hidden;
                    }

                }
                else
                {
                    ClearFields();
                }

            }

        }
        private void ClearFields()
        {
            tbBrand.Text =string.Empty;
            tbCapacity.Text = string.Empty;
            tbInterface.Text = string.Empty;
            tbFormFactor.Text = string.Empty;
            tbReedSpeed.Text = string.Empty;
            tbWriteSpeed.Text = string.Empty;
            tbPrice.Text = string.Empty;
            ImgUrl.Visibility = Visibility.Hidden;
        }
        public void SetUp(int StorageId = -1)
        {
            StorageList.Clear();
            Storage empty = new() { Id = -1, Name ="None" }; 
            StorageList.Add(empty);
            foreach (Model.DB.Storage storage in ComponentsDB.Storage.ToList())
            {
                StorageList.Add(storage);
            }
            Storage? sel = StorageList.FirstOrDefault(storage => storage.Id == StorageId);
            if (sel != null)
            {
                ListBoxStorage.SelectedItem = sel;
            }
            else
            {
                ListBoxStorage.SelectedItem = empty;
            }
        }
        private void ButtonChooseComponent_Click(object sender, RoutedEventArgs e)
        {

            StorageSelectedHandler?.Invoke(this,new SelectedStorageEventArgs(SelectedStorage));
        
        }
        
    }   
}
