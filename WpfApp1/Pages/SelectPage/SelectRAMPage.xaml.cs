﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using WpfApp1.Model.DB;

namespace WpfApp1.Pages.SelectPage
{
    public class SelectedRAMEventArgs : EventArgs
    {
        public Model.DB.RAM? RAM { get; set; }
        public SelectedRAMEventArgs(Model.DB.RAM? RAM)
        {
            this.RAM = RAM;
        }
    }
    public partial class SelectRAMPage : UserControl, INotifyPropertyChanged
    {
        public event EventHandler<SelectedRAMEventArgs>? RAMSelectedHandler;
        public event PropertyChangedEventHandler? PropertyChanged;
        public void NotifyPropertyChange([CallerMemberName] string? propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        public ObservableCollection<Model.DB.RAM> RAMList { get; set; } = [];

        public Model.DB.RAM? _selectedRAM { get; set; }
        public Model.DB.RAM? SelectedRAM
        {
            get => _selectedRAM;
            set
            {
                _selectedRAM = value;
                NotifyPropertyChange();
            }
        }

        public ComponentsContext ComponentsDB { get; set; }

        public SelectRAMPage()
        {
            DataContext = this;
            InitializeComponent();
            PropertyChanged += OnPropChange;
        }
        private void OnPropChange(object? sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(SelectedRAM))
            {
                if (SelectedRAM != null && SelectedRAM.Id != -1)
                {
                    tbBrand.Text = SelectedRAM.Brand;
                    tbRAMType.Text = (from RamType in ComponentsDB.RamTypes where RamType.Id == SelectedRAM.RamTypeId select RamType).ToList().FirstOrDefault().Name;
                    tbFrequency.Text = SelectedRAM.Frequency.ToString();
                    tbCapacity.Text = SelectedRAM.Capacity.ToString();
                    tbModules.Text = SelectedRAM.Modules.ToString();
                    tbCASLatency.Text = SelectedRAM.CASLatency.ToString();
                    tbPrice.Text = SelectedRAM.Price.ToString();

                    string? img_src = SelectedRAM.ImgUrl;
                    if (img_src != null)
                    {
                        ImgUrl.Source = new BitmapImage(new Uri(img_src));
                        ImgUrl.Visibility = Visibility.Visible;
                    }
                    else
                    {
                        ImgUrl.Visibility = Visibility.Hidden;
                    }

                }
                else
                {
                    ClearFields();
                }

            }

            
        }

        private void ClearFields()
        {
            tbBrand.Text = string.Empty;
            tbRAMType.Text = string.Empty; 
            tbFrequency.Text =  string.Empty;
            tbCapacity.Text =  string.Empty;
            tbModules.Text = string.Empty;
            tbCASLatency.Text = string.Empty;
            tbPrice.Text =  string.Empty;
            ImgUrl.Visibility = Visibility.Hidden;
        }
        public void SetUp(int motherboardId =-1, int RamId = -1)
        {
           RAMList.Clear();
            Model.DB.RAM empty = new()
            {
                Id = -1,
                Name = "None"
            };
            RAMList.Add(empty);
            RAM sel = null;

            if (motherboardId == -1)
            {
                if (RamId == -1)
                    ListBoxRAM.SelectedItem = empty;

                foreach (RAM Ram in ComponentsDB.Rams)
                {
                    RAMList.Add(Ram);
                    if (Ram.Id == RamId)
                        sel = Ram;
                }
            }
            else
            {
                if (RamId == -1)
                    ListBoxRAM.SelectedItem = empty;

                int RamTypeId = (from mb in ComponentsDB.Motherboards where mb.Id == motherboardId select mb.RamTypeId).First();
                foreach (RAM Ram in (from Ram in ComponentsDB.Rams where Ram.RamTypeId == RamTypeId select Ram))
                {
                    RAMList.Add(Ram);
                    if (Ram.Id == RamId)
                        sel = Ram;
                }
            }

            if (sel != null)
            {
                SelectedRAM = sel;
            }
        }
        private void ButtonChooseComponent_Click(object sender, RoutedEventArgs e)
        {
            RAMSelectedHandler?.Invoke(this, new SelectedRAMEventArgs(SelectedRAM));
        }
    }
    
}
