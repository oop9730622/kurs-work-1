﻿using System;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Data.SqlClient;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Media.Imaging;
using WpfApp1.Model.DB;


namespace WpfApp1.Pages.SelectPage
{
    public class SelectedMBEventArgs : EventArgs
    {
        public Model.DB.Motherboard? Motherboard { get; set; }
        public SelectedMBEventArgs(Model.DB.Motherboard? motherboard)
        {
            this.Motherboard = motherboard;
        }
    }

    public partial class SelectMBPage : UserControl, INotifyPropertyChanged
    {
        public event EventHandler<SelectedMBEventArgs>? MBSelectedHandler;
        public event PropertyChangedEventHandler? PropertyChanged;

        public void NotifyPropertyChange([CallerMemberName] string? propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public ObservableCollection<Model.DB.Motherboard> MBList { get; set; } = new ObservableCollection<Model.DB.Motherboard>();

        public Model.DB.Motherboard? SelectedMB
        {
            get => _selectedMB;
            set
            {
                _selectedMB = value;
                NotifyPropertyChange();
            }
        }

        public Model.DB.Motherboard? _selectedMB { get; set; }
        public ComponentsContext ComponentsDB { get; set; }

        public SelectMBPage()
        {
            DataContext = this;
            InitializeComponent();
            PropertyChanged += OnPropChange;
        }

        private void OnPropChange(object? sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(SelectedMB))
            {
                if (SelectedMB != null && SelectedMB.Id != -1)
                {
                    tbBrand.Text = SelectedMB.Brand;
                    tbChipset.Text = SelectedMB.Chipset.ToString();
                    tbFormFactor.Text = SelectedMB.FormFactor;
                    tbUSBPort.Text = SelectedMB.USBPorts.ToString();
                    tbRAMType.Text = (from RamType in ComponentsDB.RamTypes where RamType.Id == SelectedMB.RamTypeId select RamType).ToList().FirstOrDefault().Name;
                    tbMAXRAM.Text = SelectedMB.MaxRAM.ToString();
                    tbPCIeSlot.Text = SelectedMB.PCIeSlots.ToString();
                    tbSoket.Text = (from socket in ComponentsDB.Sockets where socket.Id == SelectedMB.SocketId select socket).ToList().FirstOrDefault().Name;
                    tbSATAPort.Text = SelectedMB.SATAPorts.ToString();
                    tbPrice.Text = SelectedMB.Price.ToString();

                    string? img_src = SelectedMB.ImgUrl;
                    if (img_src != null)
                    {
                        try
                        {
                            ImgUrl.Source = new BitmapImage(new Uri(img_src));
                            ImgUrl.Visibility = Visibility.Visible;
                        } catch { }
                    }
                    else if (img_src == null )
                    {
                        ImgUrl.Visibility = Visibility.Hidden;
                    }
                }
                else
                {
                    ClearFields();
                }
            }
        }
        private void ClearFields()
        {
            tbBrand.Text = string.Empty;
            tbChipset.Text = string.Empty;
            tbFormFactor.Text = string.Empty;
            tbUSBPort.Text = string.Empty;
            tbRAMType.Text = string.Empty;
            tbMAXRAM.Text = string.Empty;
            tbPCIeSlot.Text = string.Empty;
            tbSoket.Text = string.Empty;
            tbSATAPort.Text = string.Empty;
            tbPrice.Text = string.Empty;
            ImgUrl.Source = null;
            ImgUrl.Visibility = Visibility.Hidden;
        }

        public void SetUp(int motherboardId = -1, int cpuId = -1, int RamId = -1)
        {
            MBList.Clear();
            Model.DB.Motherboard empty = new()
            {
                Id = -1,
                Name = "None"
            };
            MBList.Add(empty);

            Model.DB.CPU cpu;
            Model.DB.Socket socket = new() { Id=-1};
            if(cpuId != -1)
            {
                cpu = (from Dbcpu in ComponentsDB.Cpus where Dbcpu.Id == cpuId select Dbcpu).First();
                socket = (from DbSocket in ComponentsDB.Sockets where DbSocket.Id == cpu.SocketId select DbSocket).First();
            }
            

            Model.DB.RAM? ram;
            Model.DB.RamType ramType = new() { Id=-1 };
            if (RamId != -1)
            {
                ram = (from DbRam in ComponentsDB.Rams where DbRam.Id == RamId select DbRam).First();
                ramType = (from DbRamType in ComponentsDB.RamTypes where DbRamType.Id == ram.RamTypeId select DbRamType).First();
            }
            
            foreach(Model.DB.Motherboard mb in (from mb in ComponentsDB.Motherboards where (cpuId == -1 || socket.Id == mb.SocketId) && (RamId == -1 || ramType.Id == mb.RamTypeId) select mb) )
                MBList.Add(mb);

            Model.DB.Motherboard? sel = MBList.FirstOrDefault(mb => mb.Id == motherboardId);
            if (sel != null)
                ListBoxMB.SelectedItem = sel;
            else 
                ListBoxMB.SelectedItem = empty;
        }


        private void ButtonChooseComponent_Click(object sender, RoutedEventArgs e)
        {
            MBSelectedHandler?.Invoke(this, new SelectedMBEventArgs(SelectedMB));
        }
    }
}
