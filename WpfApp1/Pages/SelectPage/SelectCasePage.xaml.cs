﻿using Microsoft.Identity.Client.Extensions.Msal;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using WpfApp1.Model.DB;
using WpfApp1.Pages.MenuPage;

namespace WpfApp1.Pages.SelectPage
{
    public class SelectedCaseEventArgs : EventArgs
    {
        public Model.DB.PCCase? Case { get; set; }
        public SelectedCaseEventArgs(Model.DB.PCCase? Case)
        {
            this.Case = Case;
        }
    }
    public partial class SelectCasePage : UserControl, INotifyPropertyChanged
    {
        public event EventHandler<SelectedCaseEventArgs>? CaseSelectedHandler;
        public event PropertyChangedEventHandler? PropertyChanged;
        public void NotifyPropertyChange([CallerMemberName] string? propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        public ObservableCollection<Model.DB.PCCase> CaseList { get; set; } = [];
        public ComponentsContext ComponentsDB { get; set; }

        private Model.DB.PCCase _selectedCase {  get; set; }
        public Model.DB.PCCase SelectedCase
        {
            get => _selectedCase;
            set
            {
                _selectedCase = value;
                NotifyPropertyChange();
            }
        }
        public SelectCasePage()
        {
            DataContext = this;
            InitializeComponent();
            PropertyChanged += OnPropChange;

        }
        private void OnPropChange(object? sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(SelectedCase))
            {
                if (SelectedCase != null && SelectedCase.Id != -1)
                {
                    tbBrand.Text = SelectedCase.Brand;
                    tbType.Text = SelectedCase.Type;
                    tbColor.Text = SelectedCase.Brand;
                    tbDimensoins.Text = SelectedCase.Dimensions;
                    tbFormFactor.Text = SelectedCase.FormFactor;
                    tbPrice.Text = SelectedCase.Price.ToString();
                    string? img_src = SelectedCase.ImgUrl;
                    if (img_src != null)
                    {
                        ImgUrl.Source = new BitmapImage(new Uri(img_src));
                        ImgUrl.Visibility = Visibility.Visible;
                    }
                    else
                    {
                        ImgUrl.Visibility = Visibility.Hidden;
                    }
                }
                else
                {
                    ClearFields();
                }
            }
            
        }

        private void ClearFields()
        {
            tbBrand.Text = string.Empty;
            tbType.Text = string.Empty;
            tbColor.Text = string.Empty;
            tbDimensoins.Text = string.Empty;
            tbFormFactor.Text = string.Empty;
            tbPrice.Text = string.Empty;
            ImgUrl.Visibility = Visibility.Hidden;
        }

        public void SetUp( int caseId = -1)
        {
            CaseList.Clear();
            PCCase empty = new() { Id = -1, Name ="None" };
            CaseList.Add(empty);
            foreach (Model.DB.PCCase Case in ComponentsDB.PCCase.ToList())
            {
                CaseList.Add(Case);
            }
            PCCase? sel = CaseList.FirstOrDefault(Case => Case.Id == caseId);
            if (sel != null)
            {
                ListBoxCase.SelectedItem = sel;
            }
            else
            {
                ListBoxCase.SelectedItem = empty;
            }
        }
        private void ButtonChooseComponent_Click(object sender, RoutedEventArgs e)
        {

            CaseSelectedHandler?.Invoke(this, new SelectedCaseEventArgs(SelectedCase));

        }
    }
    
}
