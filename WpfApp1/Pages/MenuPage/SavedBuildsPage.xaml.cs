﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using WpfApp1.Model.DB;
using WpfApp1.Pages.SelectPage;
using WpfApp1.ToolKit;

namespace WpfApp1.Pages.MenuPage
{
    public class SelectedBuildEventArgs : EventArgs
    {
        public Model.DB.Build Build { get; set; }
        public SelectedBuildEventArgs(Model.DB.Build build)
        {
            this.Build = build;
        }
    }

    public partial class SavedBuildsPage : UserControl, INotifyPropertyChanged
    {
        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler? PropertyChanged;
        public void NotifyPropertyChange([CallerMemberName] string? propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion

        #region EditCommand
        public ICommand EditCommand { get; set; }
        public event EventHandler<SelectedBuildEventArgs>? EditCommandExecuted;
        private void EditCommandExecute(object? arg)
        {
            EditCommandExecuted?.Invoke(this, new SelectedBuildEventArgs(SelectedBuild ?? throw new Exception("Null Build selected")));
        }
        private bool EditCommandCanExecute(object? arg)
        {
            return SelectedBuild != null;
        }
        #endregion

        #region DeleteCommand
        public ICommand DeleteCommand { get; set; }
        private void DeleteCommandExecute(object? arg)
        {
            MessageBoxResult result = MessageBox.Show("Are you sure?", "Warning", MessageBoxButton.OKCancel);
            if (result == MessageBoxResult.OK)
            {
                ComponentsDB.Builds.Remove(SelectedBuild);
                ComponentsDB.SaveChanges();
                BuildList.Remove(SelectedBuild);
                SelectedBuild = null;
            }
        }
        private bool DeleteCommandCanExecute(object? arg)
        {
            return SelectedBuild != null;
        }
        #endregion

        #region ReturnCommand
        public ICommand ReturnCommand { get; set; }
        public event EventHandler? ReturnCommandExecuted;
        private void ReturnCommandExecute(object? arg)
        {
            ReturnCommandExecuted?.Invoke(this, EventArgs.Empty);
        }
        #endregion

        public ObservableCollection<Model.DB.Build> BuildList { get; set; } = [];
        private Model.DB.Build? _selectedBuild = null;
        public Model.DB.Build? SelectedBuild
        {
            get => _selectedBuild;
            set
            {
                _selectedBuild = value;
                NotifyPropertyChange();
            }
        }
        
        private ComponentsContext? _componentsContext;
        public ComponentsContext ComponentsDB
        {
            get
            {
                if (_componentsContext == null)
                    throw new ArgumentNullException();
                return _componentsContext;
            }
            set => _componentsContext = value;
        }
        

        public SavedBuildsPage()
        {
            DataContext = this;

            EditCommand = new ToolKit.RelayCommand(EditCommandExecute, EditCommandCanExecute);
            DeleteCommand = new ToolKit.RelayCommand(DeleteCommandExecute, DeleteCommandCanExecute);
            ReturnCommand = new ToolKit.RelayCommand(ReturnCommandExecute);

            InitializeComponent();
        }
        public void SetUp()
        {            
            BuildList.Clear();
            foreach (Model.DB.Build build in ComponentsDB.Builds)
            {
                BuildList.Add(build);
            }            
        }
    }
}
