﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WpfApp1.Model.DB;
using WpfApp1.ToolKit;

namespace WpfApp1.Pages.MenuPage
{
  
    public partial class NewComponentPage : UserControl
    {
        public event EventHandler PageClosed;

        public ComponentsContext ComponentsDB {  get; set; }

        private List<DataGrid> DataGrids = [];
        private List<StackPanel> StackPanels = [];

        #region ObservableCollection
        public ObservableCollection<Model.DB.CPU> CPUList { get; set; } = [];
        public ObservableCollection<Model.DB.GPU> GPUList { get; set; } = [];
        public ObservableCollection<Model.DB.Motherboard> MBList { get; set; } = [];
        public ObservableCollection<Model.DB.RAM> RAMList { get; set; } = [];
        public ObservableCollection<Model.DB.CpuCooler> CCList { get; set; } = [];
        public ObservableCollection<Model.DB.PCCase> CaseList { get; set; } = [];
        public ObservableCollection<Model.DB.PowerSupply> PSList { get; set; } = [];
        public ObservableCollection<Model.DB.Storage> StorageList { get; set; } = [];
        #endregion
        #region Model.DB
        public Model.DB.CPU? SelectedCPU { get; set; }
        public Model.DB.GPU? SelectedGPU { get; set; }
        public Model.DB.Motherboard? SelectedMB { get; set; }
        public Model.DB.RAM? SelectedRAM { get; set; }
        public Model.DB.CpuCooler? SelectedCC { get; set; }
        public Model.DB.PowerSupply? SelectedPS { get; set; }
        public Model.DB.Storage? SelectedStorage { get; set; }
        public Model.DB.PCCase? SelectedCase { get; set; }
        #endregion
        #region CPU Commands
        public ICommand AddCommandCPU => new RelayCommand(AddItemCPU);
        public ICommand RemoveCommandCPU => new RelayCommand(RemoveItemCPU, CanRemoveItemCPU);
        private bool CanRemoveItemCPU(object? arg)
        {
            return SelectedCPU != null;
        }
        private void AddItemCPU (object? parameter) 
         {
            Model.DB.CPU cpu = new()
            {
                Name = "None",
                Brand = "None",
                Architecture = "None",
                BaseClock = 0,
                BoostClock = 0,
                Cores = 0,
                Threads = 0,
                TDP = 0,
                SocketId = 1,
                Price = 0,
                ImgUrl = "None"
            };
            CPUList.Add(cpu);
            ComponentsDB.Cpus.Add(cpu);
            ComponentsDB.SaveChanges();
         }
        private void RemoveItemCPU(object? parameter)
        {
            ComponentsDB.Cpus.Remove(SelectedCPU);
            ComponentsDB.SaveChanges();
            CPUList.Remove(SelectedCPU);
        }
        #endregion
        #region GPU Commands
        public ICommand AddCommandGPU => new RelayCommand(AddItemGPU);
        public ICommand RemoveCommandGPU => new RelayCommand(RemoveItemGPU, CanRemoveItemGPU);
        private bool CanRemoveItemGPU(object? arg)
        {
            return SelectedGPU != null;
        }
        private void AddItemGPU(object? parameter)
        {
            Model.DB.GPU gpu = new()
            {
                Name = "None",
                Brand = "None",
                Chipset = "None",
                MemoryCapacity = 0,
                MemoryType = "None",
                TDP = 0,
                BaseClock = 0,
                BoostClock = 0,
                Architecture = "None",
                Price = 0,
                ImgUrl = "None"
            };
            GPUList.Add(gpu);
            ComponentsDB.Gpus.Add(gpu);
            ComponentsDB.SaveChanges();
        }
        private void RemoveItemGPU(object? parameter)
        {
            ComponentsDB.Gpus.Remove(SelectedGPU);
            ComponentsDB.SaveChanges();
            GPUList.Remove(SelectedGPU);
        }
        #endregion
        #region RAM Commands
        public ICommand AddCommandRAM => new RelayCommand(AddItemRAM);
        public ICommand RemoveCommandRAM => new RelayCommand(RemoveItemRAM, CanRemoveItemRAM);
        private bool CanRemoveItemRAM(object? arg)
        {
            return SelectedRAM != null;
        }
        private void AddItemRAM(object? parameter)
        {
            Model.DB.RAM ram = new()
            {
                Name = "None",
                Brand = "None",
                RamTypeId = 1,
                Frequency = 0,
                Capacity = 0,
                Modules = 0,
                CASLatency = 0,
                Price = 0,
                ImgUrl = "None"
            };
            RAMList.Add(ram);
            ComponentsDB.Rams.Add(ram);
            ComponentsDB.SaveChanges();
        }
        private void RemoveItemRAM(object? parameter)
        {
            ComponentsDB.Rams.Remove(SelectedRAM);
            ComponentsDB.SaveChanges();
            RAMList.Remove(SelectedRAM);
        }
        #endregion
        #region MB Commands
        public ICommand AddCommandMB => new RelayCommand(AddItemMB);
        public ICommand RemoveCommandMB => new RelayCommand(RemoveItemMB, CanRemoveItemMB);
        private bool CanRemoveItemMB(object? arg)
        {
            return SelectedMB != null;
        }
        private void AddItemMB(object? parameter)
        {
            Model.DB.Motherboard motherboard = new()
            {
                Name = "None",
                Brand = "None",
                Chipset = "None",
                FormFactor = "None",
                SocketId = 1,
                RamTypeId = 1,
                MaxRAM = 0,
                PCIeSlots = 0,
                USBPorts = 0,
                SATAPorts = 0,
                Price = 0,
                ImgUrl = "None"
            };
            MBList.Add(motherboard);
            ComponentsDB.Motherboards.Add(motherboard);
            ComponentsDB.SaveChanges();
        }
        private void RemoveItemMB(object? parameter)
        {
            ComponentsDB.Motherboards.Remove(SelectedMB);
            ComponentsDB.SaveChanges();
            MBList.Remove(SelectedMB);
        }
        #endregion
        #region Case Commands
        public ICommand AddCommandCase => new RelayCommand(AddItemCase);
        public ICommand RemoveCommandCase => new RelayCommand(RemoveItemCase, CanRemoveItemCase);
        private bool CanRemoveItemCase(object? arg)
        {
            return SelectedCase != null;
        }
        private void AddItemCase(object? parameter)
        {
            Model.DB.PCCase PCсase = new()
            {
                Name = "None",
                Brand = "None",
                Type = "None",
                FormFactor = "None",
                Color = "None",
                Dimensions = "None",
                Price = 0,
                ImgUrl = "None"
            };
            CaseList.Add(PCсase);
            ComponentsDB.PCCase.Add(PCсase);
            ComponentsDB.SaveChanges();
        }
        private void RemoveItemCase(object? parameter)
        {
            ComponentsDB.PCCase.Remove(SelectedCase);
            ComponentsDB.SaveChanges();
            CaseList.Remove(SelectedCase);
        }
        #endregion
        #region Cooling Commands
        public ICommand AddCommandCC => new RelayCommand(AddItemCC);
        public ICommand RemoveCommandCC => new RelayCommand(RemoveItemCC, CanRemoveItemCC);
        private bool CanRemoveItemCC(object? arg)
        {
            return SelectedCC != null;
        }
        private void AddItemCC(object? parameter)
        {
            Model.DB.CpuCooler CC = new()
            {
                Name = "None",
                Brand = "None",
                Type = "None",
                SocketId = 1,
                FanSpeed = 0,
                NoiseLevel = 0,
                Airflow = 0,
                Dimensions = "None",
                Price = 0,
                ImgUrl = "None"
            };
            CCList.Add(CC);
            ComponentsDB.CpuCooler.Add(CC);
            ComponentsDB.SaveChanges();
        }
        private void RemoveItemCC(object? parameter)
        {
            ComponentsDB.CpuCooler.Remove(SelectedCC);
            ComponentsDB.SaveChanges();
            CCList.Remove(SelectedCC);
        }
        #endregion
        #region Power Commands
        public ICommand AddCommandPS => new RelayCommand(AddItemPS);
        public ICommand RemoveCommandPS => new RelayCommand(RemoveItemPS, CanRemoveItemPS);
        private bool CanRemoveItemPS(object? arg)
        {
            return SelectedPS != null;
        }
        private void AddItemPS(object? parameter)
        {
            Model.DB.PowerSupply power = new()
            {
                Name = "None",
                Brand = "None",
                Wattage = 0,
                Efficiency ="None",
                Modularity = "None",
                FormFactor = "None",
                ConnectorTypes = "None",
                Price = 0,
                ImgUrl = "None"
            };
            PSList.Add(power);
            ComponentsDB.PowerSupply.Add(power);
            ComponentsDB.SaveChanges();
        }
        private void RemoveItemPS(object? parameter)
        {
            ComponentsDB.PowerSupply.Remove(SelectedPS);
            ComponentsDB.SaveChanges();
            PSList.Remove(SelectedPS);
        }
        #endregion
        #region Storage Commands
        public ICommand AddCommandStorage => new RelayCommand(AddItemStorage);
        public ICommand RemoveCommandStorage => new RelayCommand(RemoveItemStorage, CanRemoveItemStorage);
        private bool CanRemoveItemStorage(object? arg)
        {
            return SelectedStorage != null;
        }
        private void AddItemStorage(object? parameter)
        {
            Model.DB.Storage storage = new()
            {
                Name = "None",
                Brand = "None",
                Capacity = "None",
                Interface = "None",
                FormFactor = "None",
                ReadSpeed = 0,
                WriteSpeed = 0,
                Price = 0,
                ImgUrl = "None"
            };
            StorageList.Add(storage);
            ComponentsDB.Storage.Add(storage);
            ComponentsDB.SaveChanges();
        }
        private void RemoveItemStorage(object? parameter)
        {
            ComponentsDB.Storage.Remove(SelectedStorage);
            ComponentsDB.SaveChanges();
            StorageList.Remove(SelectedStorage);
        }
        #endregion
        #region SetUp
        private void  SetUpData()
        {
            DataGrids.Add(DataGridCPU);
            DataGrids.Add(DataGridGPU);
            DataGrids.Add(DataGridMB);
            DataGrids.Add(DataGridRAM);
            DataGrids.Add(DataGridCC);
            DataGrids.Add(DataGridStorage);
            DataGrids.Add(DataGridPS);
            DataGrids.Add(DataGridCase);
        }
        private void SetUpPanels() 
        {
            StackPanels.Add(StackPanelCPU);
            StackPanels.Add(StackPanelGPU);
            StackPanels.Add(StackPanelMB);
            StackPanels.Add(StackPanelRAM);
            StackPanels.Add(StackPanelStorage);
            StackPanels.Add(StackPanelPS);
            StackPanels.Add(StackPanelCC);
            StackPanels.Add(StackPanelCase);
            StackPanels.Add(Fake);
        }
        public void SetUp()
        {
            CPUList.Clear();
            foreach(Model.DB.CPU cpu in ComponentsDB.Cpus)
            {
                CPUList.Add(cpu);
            }
            GPUList.Clear();
            foreach (Model.DB.GPU gpu in ComponentsDB.Gpus)
            {
               GPUList.Add(gpu);
            }
            MBList.Clear();
            foreach (Model.DB.Motherboard motherboard in ComponentsDB.Motherboards)
            {
                MBList.Add(motherboard);
            }
            foreach (Model.DB.RAM ram in ComponentsDB.Rams)
            {
                RAMList.Add(ram);
            }
            foreach (PCCase PCcase in ComponentsDB.PCCase)
            {
                CaseList.Add(PCcase);
            }
            foreach (Model.DB.CpuCooler cpuCooler in ComponentsDB.CpuCooler)
            {
               CCList.Add(cpuCooler);
            }
            foreach (Model.DB.PowerSupply power in ComponentsDB.PowerSupply)
            {
               PSList.Add(power);
            }
            foreach (Model.DB.Storage storage in ComponentsDB.Storage)
            {
                StorageList.Add(storage);
            }
        }
        #endregion
        public NewComponentPage()
        {
            DataContext = this;
            InitializeComponent();
            SetUpData();
            SetUpPanels();
       }
        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var combobox = sender as ComboBox;
            if (combobox != null)
            {
                foreach (DataGrid dataGrid in DataGrids)
                {
                    string Tag = dataGrid.Tag as string;
                    ComboBoxItem selItem = combobox.SelectedItem as ComboBoxItem;
                    if (Tag == selItem.Content as string)
                    {
                        dataGrid.Visibility = Visibility.Visible;
                    }
                    else
                    {
                        dataGrid.Visibility = Visibility.Hidden;
                    }
                }
                foreach (StackPanel stackPanel in StackPanels)
                {
                    string Tag = stackPanel.Tag as string;
                    ComboBoxItem selItem = combobox.SelectedItem as ComboBoxItem;
                    if (Tag == selItem.Content as string)
                    {
                        stackPanel.Visibility = Visibility.Visible;
                    }
                    else
                    {
                        stackPanel.Visibility = Visibility.Hidden;
                    }
                }
            }
        }
        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            ComponentsDB.SaveChanges();
            PageClosed?.Invoke(this, EventArgs.Empty);
        }
    }
}
