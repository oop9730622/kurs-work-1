﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfApp1.Model.DB;

namespace WpfApp1
{
    public class ComponentsContext : DbContext
    {
        public ComponentsContext() : base() { }
        public DbSet<CPU> Cpus { get; set; }
        public DbSet<GPU> Gpus { get; set; }
        public DbSet<Socket> Sockets { get; set; }
        public DbSet<RamType> RamTypes { get; set; }
        public DbSet<Motherboard> Motherboards { get; set; }
        public DbSet<RAM> Rams { get; set; }
        public DbSet<CpuCooler> CpuCooler { get; set; }
        public DbSet<PowerSupply> PowerSupply { get; set; }
        public DbSet<Storage> Storage { get; set; }
        public DbSet<PCCase?> PCCase { get; set; }
        public DbSet<Build> Builds { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            options.UseSqlServer("Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=C:\\Users\\user\\Documents\\ComponentsDB.mdf;Integrated Security=True;Connect Timeout=30;Encrypt=True");
        }
    }


}
