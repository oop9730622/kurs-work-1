﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp1.Model.DB
{
    public class Build
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public int cpusId { get; set; }
        public int gpusId { get; set; }
        public int MotherboardsId { get; set; }
        public int PowerSuplyId{ get; set; }
        public int RamsId { get; set; }
        public int StorageId {  get; set; }
        public int caseId { get; set; }
        public int CpuCoolerId {  get; set; }
        public double BuildTotalPrice {  get; set; }

     }
}
