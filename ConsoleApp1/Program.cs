﻿using System;
using System.Text;

static class MatrixOperations
{
    static int[,] matrix;
    static int size;

    static int Main(string[] args)
    {
        Console.OutputEncoding = Encoding.Unicode;
        Console.InputEncoding = Encoding.Unicode;
        do
        {
            Console.Write("Введіть розмір квадратної матриці: ");
            string input = Console.ReadLine();

            if (!int.TryParse(input, out size) || size <= 0)
            {
                Console.WriteLine("Неправильний формат розміру матриці.");
            }
        } while (size <= 0);

        matrix = new int[size, size];

        while (true)
        {
            Console.WriteLine("\nМеню:");
            Console.WriteLine("1. Ініціалізація матриці");
            Console.WriteLine("2. Вивід матриці на екран");
            Console.WriteLine("3. Максимальний і мінімальний елемент на головній діагоналі");
            Console.WriteLine("4. Сортування елементів за зростанням у кожному рядку");
            Console.WriteLine("5. Вихід");

            Console.Write("Введіть номер операції: ");
            int option;
            if (!int.TryParse(Console.ReadLine(), out option))
            {
                Console.WriteLine("Неправильний формат вводу.");
                continue;
            }

            switch (option)
            {
                case 1:
                    InitializeMatrix();
                    break;
                case 2:
                    PrintMatrix();
                    break;
                case 3:
                    FindMinMaxDiagonalElements();
                    break;
                case 4:
                    SortRows();
                    break;
                case 5:
                    Environment.Exit(0);
                    break;
                default:
                    Console.WriteLine("Неправильна операція!");
                    break;
            }
        }
        return 0;


        static void InitializeMatrix()
        {
            Random random = new Random();

            Console.WriteLine("Матриця:");

            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    matrix[i, j] = random.Next(0, 101);
                }
            }

            Console.WriteLine("Матриця ініціалізована випадковими числами.");
        }


        static void PrintMatrix()
        {
            Console.WriteLine("Матриця:");
            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    Console.Write($"{matrix[i, j]}\t");
                }
                Console.WriteLine();
            }
        }

        static void FindMinMaxDiagonalElements()
        {
            int max = matrix[0, 0], min = matrix[0, 0];

            for (int i = 0; i < size; i++)
            {
                if (matrix[i, i] > max)
                {
                    max = matrix[i, i];
                }
                if (matrix[i, i] < min)
                {
                    min = matrix[i, i];
                }
            }

            Console.WriteLine($"Максимальний елемент на головній діагоналі: {max}");
            Console.WriteLine($"Мінімальний елемент на головній діагоналі: {min}");
        }

        static void SortRows()
        {
            for (int i = 0; i < size; i++)
            {
                // Отримання i-го рядка матриці
                int[] row = new int[size];
                for (int j = 0; j < size; j++)
                {
                    row[j] = matrix[i, j];
                }

                // Сортування рядка
                Array.Sort(row);

                // Повернення відсортованого рядка у вихідну матрицю
                for (int j = 0; j < size; j++)
                {
                    matrix[i, j] = row[j];
                }
            }

            Console.WriteLine("Матриця після сортування рядків:");
            PrintMatrix();
        }
    }
}